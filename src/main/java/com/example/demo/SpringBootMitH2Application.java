package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMitH2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMitH2Application.class, args);
	}

}
